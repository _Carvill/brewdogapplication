function buildSlideshow() {
  $('.home-slideshow').cycle({
      fx: 'fade',
      slides: 'img',
      timeout: 12000,
      overlay: '> .cycle-overlay',
      overlayTemplate: '<div style="overlay"><div class="slide-title">{{title}}</div><div class="slide-description">{{desc}}</div></div>',
  });
}

function initCycle() {
    var width = $(document).width();
    var visibleSlides = 5;
    if ( width < 400 ) {visibleSlides = 1}
    else if(width < 700) {visibleSlides = 3}
    else {visibleSlides = 5};
    buildSlideshow();
}

$(document).ready(function(){
    initCycle();
});
