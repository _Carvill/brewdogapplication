
function loadApiData(){
    //Get data from external API
    var getBrewJson = $.getJSON( "https://api.punkapi.com/v2/beers", function() {
      console.log( "success" );
    });

    getBrewJson.complete(function() {
      //Once complete parse the JSON into a Javascript Array
      getBrewJson = $.parseJSON(getBrewJson.responseText);
      console.log(getBrewJson);
      buildAccordion(getBrewJson);
    });

    //If the API call was unsucessful show error message
    getBrewJson.error(function() {
      getBrewJson = getBrewJson.responseText;
      console.log( "Couldnt get data from API" );
      console.log(getBrewJson);
    });
}

function buildAccordion(getBrewJson){
  //Start building the accordion with the API data.
  console.log("Start building the acordion");

  //Iterate through each object.
  $.each(getBrewJson, function(beer, i) {

    //Setting a template layout for the accordion
    //(Note: this is not my prefered method of creating an accordion, see other examples for jQuery and pure HTML and CSS approach)
    var template = "<h4 class='accordion-toggle'>##Name## <span class='abv'>##Abv##%</span></h4><div class='accordion-content'><p class='tagline'>##TagLine##</p><p class='description'>##Description##</p><p class='brew-date'>First Brewed: ##FirstBrewed##</p></div>";
    template = template
    .replace("##Name##", i.name)
    .replace("##Description##", i.description)
    .replace("##Abv##", i.abv)
    .replace("##FoodParing##", i.food_paring)
    .replace("##FirstBrewed##", i.first_brewed)
    .replace("##Ingredients##", i.ingredients)
    .replace("##TagLine##", i.tagline);

      $( "#accordion" ).append(template);
      });
      // Locate div with ID 'accordion' and run function when divs with the class 'accordion-toggle' clicked.
      $('#accordion').find('.accordion-toggle').click(function(){
        // Expand/collapse this panel
        $(this).next().slideToggle('fast');
        //Hide the rest
        $(".accordion-content").not($(this).next()).slideUp('fast');
      });
}

$(document).ready(function($) {
  //When document is ready run the loadApiData function.
  loadApiData();
});
