var brewdogController = angular.module('brewdogController',[]);

brewdogController.controller('MainController', ['$scope', '$http', function($scope, $http){

  //Get the data from the PunkApi API.
  var getBrewData = $http.get("https://api.punkapi.com/v2/beers");

  //No error handling has been added here, in usual circumstance this would be considered.

  //Function for assinging the data using scope.
  function loadData(){
    getBrewData
    .then(function(response) {
        $scope.brewData = response.data;
      });
  }

  // When document is ready call the functions
  $(document).ready(function(){
      initCycle();
      loadData();
  });
}]);
