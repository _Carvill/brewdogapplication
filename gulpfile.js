var gulp = require('gulp');
var scss = require('gulp-sass');
var concat = require('gulp-concat');

//Concatinating SCSS files together and saving in style.css.
gulp.task('scss', function(){
  return gulp.src('./scss/*.scss')
  .pipe(scss())
  .pipe(concat('style.css'))
  .pipe(gulp.dest('./css'));
});

//Watching for changes in files, if so run task
gulp.task('watch', function(){
  gulp.watch('./scss/components/*.scss', ['scss']);
});

//Default task.
gulp.task('default', ['scss', 'watch']);
