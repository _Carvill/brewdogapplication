# README #

A simple web application that provides data on beer that Brew Dog offer using the PunkApi API: https://punkapi.com/

## Accordion ##

Note: There are multiple methods for creating accordions in Angular, In a usual scenario when not using Angular I would build purely in HTML / CSS, due to the nature of this technical task I have developed and included the accordion in this format and in jQuery.

(Note: There are easier methods for creating accordions in Angular, I opted for this method for the sake of the technical task.)

## Carousel ##
A very simple carousel added, with random content taken from Brewdogs website.

## CTA Grid of beers ##
Grid of beers returned from the API, sortable by name, highest percentage, lowest percentage and first brewed. The grid is responsive and stacks when viewed on mobile devices

## Compression / Optimisation ##
SCSS concatination and minification using a Gulp tasks.

## Styling / Design ##
Style and design has been taken from the BrewDog brand, due to time constraints some styling might appear off/unfinished.

Very hesitant to hand this over without it being complete.


I would like to thank you again for your time and if you have any questions feel free to get in contact.
